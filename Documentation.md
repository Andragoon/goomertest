# Test Goomer Project
[Link do projeto](https://bitbucket.org/Andragoon/goomertest/src/master/)
```
git clone git@bitbucket.org:Andragoon/goomertest.git
```

## Index
  1. **Setup**
  2. **Requisitos**
  3. **Estrutura**
  4. **Considerações**

## 1. Setup

O para construção do projeto busquei libs que simplificasse algumas funções como as requisições e o download de imagens.
Para obter os json foi usada a lib Retrofit juntamente com Gson, com elas é possível fazer a requisição http e a tratativa do json, de forma simples e sem uma geração muito grande de código.
O Picasso foi outra lib usada para o load the image url e adição em ImageView com uma sintaxe fácil e simples torna muito mais rápido do que uma estruturação de AsyncTask.
```java

Picasso.get().load("URL").fit().centerCrop().into(myImageView);

```    

E para a construções das lista e grid view foi usado as libs cardview, essa para a construção dos itens e recyclerview para criação de views mais leves e recomendadas pela Google.

```java
  //Config the retrofit and gson
  implementation 'com.squareup.retrofit2:converter-gson:2.5.0'

  //Picasso
  implementation 'com.squareup.picasso:picasso:2.71828'

  //CardView
  implementation 'com.android.support:cardview-v7:28.0.0'

  //RecyclerView
  implementation 'com.android.support:recyclerview-v7:28.0.0'
```



## 2. Requisitos

  O projeto se trata de fazer uma requisição e tratar as informações em views do android. Para isso:

  * O programa deve fazer uma request para obter os restaurant;
    - http://challange.goomer.com.br/restaurants
  * O programa deve fazer uma request para buscar os menus relacionados a um restaurante especifico;
    - http://challange.goomer.com.br/restaurants/{id}/menu
  * O programa deve fazer o parse das informações do json restaurants;
    ```javascript
      {
        0 
        id  1
        name  "Cupcake para todos"
        address "Rua dos Sonhos, 310"
        image "https://images.unsplash.com/photo-1525640788966-69bdb028aa73?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d53c30ba55d9ca863d57fabfffdb416b&auto=format&fit=crop&w=1047&q=80" 
      }
    ```
  * O programa deve fazer o parse das informações do json menu;
    ```javascript
      {
        0 
        restaurantId  1
        name  "Brigadeiro de padaria"
        image "https://st4.depositphotos.com/1077028/21147/i/1600/depositphotos_211470810-stock-photo-brigadeiro-brazilian-famous-chocolate-candy.jpg"
        price 8
        group "Doces"
      }
    ```
  * O programa deve criar uma lista de restaurantes com as informações obtidas;
  * Ao clicar em um restaurante o programa deve fazer a busca dos menus relacionados aquele restaurant_id;
    - Caso haja menus criar uma gridview com as informações do json menu;
    - Caso não haja mostrar a mensagem para o usuário;
  * O programa deve verificar se há uma conexão com internet;


## 3. Estrutura
  
  1. **Código**
    Para esse projeto busquei usar a arquitetura MVP, na qual temos duas classes de model [Restaurant](https://bitbucket.org/Andragoon/goomertest/raw/cdf56272b22b02fc5348f585b709999c23be631e/app/src/main/java/com/example/leonardo/testgoomer/model/Restaurant.java) e [RestaurantMenu](https://bitbucket.org/Andragoon/goomertest/raw/cdf56272b22b02fc5348f585b709999c23be631e/app/src/main/java/com/example/leonardo/testgoomer/model/RestaurantMenu.java) nessas estão os atributos que devem ser parseados.
    Nesse projeto as activities são classes de criação do layout e seus adapters entram como as Presenter onde toda a lógica está presente. Exceção feita a classe Splash que tem uma classe presenter, a Splash é responsável por fazer o GET dos restaurantes.
  
  2. **Flow**
    O fluxo do projeto pode ser visto no Marvel abaixo:
    [Test Goomer](https://marvelapp.com/57ee8j3)

## 4. Considerações
  
  O projeto parece algo simples, mas que dá uma liberdade ao desenvolvedor e tende a trazer como ele vê uma proposta de projeto.
  Sem muito tempo, infelizmente não pude fazer a persistência de dados em um banco local para um funcionamento sem internet. Outro ponto que deixei a desejar foi na implementação das injeções de dependências, poderia usar para objetos de network usando a lib Dagger2 e para injeção de layout com ButterKnife, são boas práticas e fiquei devendo nessa.
  **Obrigado pelo desafio.**
