package com.example.leonardo.testgoomer.network.restaurant;

import com.example.leonardo.testgoomer.model.Restaurant;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Class to treat the Restaurant JSON with Gson on Retrofit request.
 */
public class RestaurantTreat implements JsonDeserializer<Restaurant> {
    @Override
    public Restaurant deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonElement restaurant = json.deepCopy();

        return (new Gson().fromJson(restaurant, Restaurant.class));
    }
}
