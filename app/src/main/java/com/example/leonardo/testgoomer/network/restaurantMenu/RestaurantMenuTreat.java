package com.example.leonardo.testgoomer.network.restaurantMenu;

import com.example.leonardo.testgoomer.model.RestaurantMenu;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class RestaurantMenuTreat implements JsonDeserializer<RestaurantMenu> {
    @Override
    public RestaurantMenu deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement menu = json.deepCopy();
        return (new Gson().fromJson(menu, RestaurantMenu.class));
    }
}
