package com.example.leonardo.testgoomer.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseNetwork {
    public static final String BASE_URL = "http://challange.goomer.com.br";



    /**
     * Treat the gson to interpret response request.
     * @param objClass class model
     * @param treatClass deserialize class
     * @return Return the gson using model and the treat deserialize class
     */
    protected Gson getGson(Object objClass, Object treatClass){
        return new GsonBuilder().registerTypeAdapter(objClass.getClass(),
                treatClass)
                .create();
    }

    /**
     * Prepare the request base with the interpret
     * @param gson kind of object that want treat.
     * @return a retrofit object to do the request.
     */
    protected Retrofit myRequest(Gson gson){
        return new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.
                create(gson)).build();
    }
}
