package com.example.leonardo.testgoomer.network.restaurant;

import com.example.leonardo.testgoomer.model.Restaurant;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestaurantAPI {
    @GET("/restaurants")
    Call<List<Restaurant>> getRestaurants();
}
