package com.example.leonardo.testgoomer.activities;

import android.os.Bundle;
import android.os.Handler;

import com.example.leonardo.testgoomer.R;
import com.example.leonardo.testgoomer.presenter.SplashPresenter;
import com.example.leonardo.testgoomer.util.ConnectionUtil;

/**
 * Splash activity responsible to do the request.
 */
public class SplashScreen extends BaseActivity {
    SplashPresenter splashPresenter;
    Handler splashEffect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        splashEffect = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Verify that mobile have a connection
        if (ConnectionUtil.getConnectivityStatus(this) ==
                ConnectionUtil.NETWORK_STATUS_NOT_CONNECTED) {

            setContentView(R.layout.no_connection);

        } else {

            splashPresenter = new SplashPresenter(this);
            splashEffect.postDelayed(new Runnable() {
                @Override
                public void run() {
                    splashPresenter.goToMain();
                }
            }, 1000);
        }
    }
}
