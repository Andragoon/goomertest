package com.example.leonardo.testgoomer.network.restaurantMenu;

import android.util.Log;

import com.example.leonardo.testgoomer.model.RestaurantMenu;
import com.example.leonardo.testgoomer.network.BaseNetwork;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The class responsible for the execute RestaurantMenu request.
 */
public class RestaurantMenuNetwork extends BaseNetwork  implements Callback<List<RestaurantMenu>> {
    //Get json objects;
    Gson menuGson;
    //To do request
    RestaurantMenuAPI menuAPI;
    //To execute request
    Call<List<RestaurantMenu>> call;
    //Objects treats to use.
    private List<RestaurantMenu> menus;

    public RestaurantMenuNetwork(){
        menuGson = getGson(RestaurantMenu.class, new RestaurantMenuTreat());
        menuAPI = myRequest(menuGson).create(RestaurantMenuAPI.class);
    }

    public void doRequestAsync(int restaurantId){
        call = menuAPI.getMenuRestaurant(String.valueOf(restaurantId));
        call.enqueue(this);
    }

    public Thread doRequestSync(int restaurantId){
        call = menuAPI.getMenuRestaurant(String.valueOf(restaurantId));
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    setMenus(call.execute().body());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onResponse(Call<List<RestaurantMenu>> call,
                           Response<List<RestaurantMenu>> response) {
        setMenus(response.body());
//        for (RestaurantMenu menu: response.body()) {
//
//            Log.i(" MENUS ", menu.getName());
//        }
    }

    @Override
    public void onFailure(Call<List<RestaurantMenu>> call, Throwable t) {

    }

    public List<RestaurantMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<RestaurantMenu> menus) {
        this.menus = menus;
    }
}
