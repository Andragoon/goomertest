package com.example.leonardo.testgoomer.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.leonardo.testgoomer.R;
import com.example.leonardo.testgoomer.model.RestaurantMenu;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Copyright 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public class MenuListAdapter extends RecyclerView
        .Adapter<MenuListAdapter.MenuHolder> {

    private static ArrayList<RestaurantMenu> menuList;
    Context context;
    Drawable defautImage;


    public MenuListAdapter(ArrayList<RestaurantMenu> restaurantList, Context context) {
        this.menuList = restaurantList;
        this.context = context;
    }

    public static class MenuHolder extends RecyclerView.ViewHolder {

        TextView menuName;
        TextView menuPrice;
        ImageView menuImage;

        public MenuHolder(View itemView) {
            super(itemView);
            menuName = (TextView) itemView.findViewById(R.id.menu_name);
            menuPrice = (TextView) itemView.findViewById(R.id.menu_price);
            menuImage = (ImageView) itemView.findViewById(R.id.menu_image);
        }
    }

    @Override
    public MenuHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_card_veiw_item, parent, false);

        defautImage = parent.getContext().getResources().getDrawable(R.drawable.pratro_image);

        MenuHolder menuHolder = new MenuHolder(view);
        return menuHolder;
    }

    @Override
    public void onBindViewHolder(MenuHolder holder, final int position) {
        holder.menuName.setText(menuList.get(position).getName());

        setPriceText(holder, position);

        setImage(holder, position);

    }

    private void setImage(MenuHolder holder, int position) {
        if (menuList.get(position).getImage() != null) {
            Picasso.get()
                    .load(menuList.get(position).getImage())
                    .placeholder(R.drawable.pratro_image)
                    .fit().centerCrop().into(holder.menuImage);
        } else {
            holder.menuImage.setImageDrawable(defautImage);
        }
    }

    private void setPriceText(MenuHolder holder, int position) {
        if (splitPrice(String.valueOf(menuList.get(position).getPrice())).length() < 2 ) {
            holder.menuPrice.setText("R$ " + formatPrice(menuList.get(position).getPrice()) + "0");
        } else {
            holder.menuPrice.setText("R$ " + formatPrice(menuList.get(position).getPrice()));
        }
    }

    public String formatPrice(double price) {
        return String.format(String.valueOf(price), "%.2f%");
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }


    public String splitPrice(String price) {
        String[] parts = price.split("\\.");
        String cents = parts[1];
        return cents;
    }
}
