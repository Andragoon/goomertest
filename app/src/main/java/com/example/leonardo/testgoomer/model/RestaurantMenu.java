package com.example.leonardo.testgoomer.model;

import java.io.Serializable;

public class RestaurantMenu implements Serializable {
    private int restaurantId;
    private String name;
    private String image;
    private double price;
    //Kind of food.
    private String group;

    public RestaurantMenu(int restaurantId, String name, String image, double price, String group) {
        this.restaurantId = restaurantId;
        this.name = name;
        this.image = image;
        this.price = price;
        this.group = group;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
