package com.example.leonardo.testgoomer.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leonardo.testgoomer.R;
import com.example.leonardo.testgoomer.activities.MenuList;
import com.example.leonardo.testgoomer.model.Restaurant;
import com.example.leonardo.testgoomer.model.RestaurantMenu;
import com.example.leonardo.testgoomer.network.restaurantMenu.RestaurantMenuNetwork;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class RestaurantListAdapter extends RecyclerView
        .Adapter<RestaurantListAdapter.RestaurantHolder> {

    private static ArrayList<Restaurant> restaurantList;
    Context context;
    Thread menuThread;
    RestaurantMenuNetwork menuNetwork;
    ArrayList<RestaurantMenu> menuList;


    public RestaurantListAdapter(ArrayList<Restaurant> restaurantList, Context context) {
        this.restaurantList = restaurantList;
        this.context = context;
    }

    public static class RestaurantHolder extends RecyclerView.ViewHolder {

        TextView restaurantName;
        TextView restaurantAddress;
        ImageView restaurantImage;

        public RestaurantHolder(View itemView) {
            super(itemView);
            restaurantName = (TextView) itemView.findViewById(R.id.restaurant_name);
            restaurantAddress = (TextView) itemView.findViewById(R.id.restaurant_address);
            restaurantImage = (ImageView) itemView.findViewById(R.id.restaurant_image);
        }
    }

    @Override
    public RestaurantHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_card_view_item, parent, false);

        RestaurantHolder dataObjectHolder = new RestaurantHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(RestaurantHolder holder, final int position) {

        holder.restaurantName.setText(restaurantList.get(position).getName());
        holder.restaurantAddress.setText(restaurantList.get(position).getAddress());
        Picasso.get().load(restaurantList.get(position).getImage())
                .fit().centerCrop().into(holder.restaurantImage);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRestaurantMenu(restaurantList.get(position).getId());
                if (menuList.size() != 0) {
                    Intent intent = new Intent(context, MenuList.class);
                    intent.putExtra(String.valueOf(R.string.saved_menu), menuList);
                    intent.putExtra("restaurantName", restaurantList.get(position).getName());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Sem cardápio", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }


    public void getRestaurantMenu(int restaurantId) {
        menuNetwork = new RestaurantMenuNetwork();
        menuThread = menuNetwork.doRequestSync(restaurantId);
        menuThread.start();
        try {
            menuThread.join();
            menuList = (ArrayList<RestaurantMenu>) menuNetwork.getMenus();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}