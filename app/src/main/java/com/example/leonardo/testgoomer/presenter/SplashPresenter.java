package com.example.leonardo.testgoomer.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.leonardo.testgoomer.MainActivity;
import com.example.leonardo.testgoomer.R;
import com.example.leonardo.testgoomer.activities.RestaurantList;
import com.example.leonardo.testgoomer.model.Restaurant;
import com.example.leonardo.testgoomer.network.restaurant.RestaurantNetwork;

import java.util.ArrayList;

public class SplashPresenter {
    Context currentContex;
    Thread restaurantsT, menusT;

    final RestaurantNetwork restaurantNetwork;

    View view;

    ArrayList<Restaurant> restaurantsList;

    public SplashPresenter(Context context) {
        currentContex = context;

        //Networks
        restaurantNetwork = new RestaurantNetwork();

        view = ((Activity) currentContex).getWindow().getDecorView();

        restaurantsList = new ArrayList<>();

        getRestaurants();
    }

    public void getRestaurants() {
        restaurantsT = restaurantNetwork.doRequestSync();
        restaurantsT.start();
        try {
            restaurantsT.join();
            restaurantsList = (ArrayList<Restaurant>) restaurantNetwork.getRestaurants();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void goToMain() {
        Intent intent = new Intent(currentContex.getApplicationContext(), RestaurantList.class);
        intent.putExtra(String.valueOf(R.string.saved_restaurants), restaurantsList);
        currentContex.startActivity(intent);
    }
}
