package com.example.leonardo.testgoomer.network.restaurant;

import com.example.leonardo.testgoomer.model.Restaurant;
import com.example.leonardo.testgoomer.network.BaseNetwork;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The class responsible for the execute Restaurants request.
 */
public class RestaurantNetwork extends BaseNetwork implements Callback<List<Restaurant>> {
    //Get json objects;
    Gson restaurantGson;
    //To do request
    RestaurantAPI restaurantAPI;
    //To execute request
    Call<List<Restaurant>> call;
    //Objects treats to use.
    private List<Restaurant> restaurants;


    public RestaurantNetwork() {
        restaurantGson = getGson(Restaurant.class, new RestaurantTreat());
        restaurantAPI = myRequest(restaurantGson).create(RestaurantAPI.class);
    }

    public void doRequestAsync() {
        call = restaurantAPI.getRestaurants();
        call.enqueue(this);
    }

    public Thread doRequestSync() {
        call = restaurantAPI.getRestaurants();
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    setRestaurants(call.execute().body());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public boolean execute() {
        return call.isExecuted();
    }

    @Override
    public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
        setRestaurants(response.body());
    }

    @Override
    public void onFailure(Call<List<Restaurant>> call, Throwable t) {

    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }
}
