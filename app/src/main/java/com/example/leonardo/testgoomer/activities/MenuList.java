package com.example.leonardo.testgoomer.activities;

import android.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.leonardo.testgoomer.R;
import com.example.leonardo.testgoomer.adapters.MenuListAdapter;
import com.example.leonardo.testgoomer.model.RestaurantMenu;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MenuList extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Bundle extras;
    private ArrayList<RestaurantMenu> menus;
    private String restaurantName;
    private TextView restaurantTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_restaurant_menu_list);
        extras = getIntent().getExtras();
        menus = (ArrayList<RestaurantMenu>) extras
                .getSerializable(String.valueOf(R.string.saved_menu));
        restaurantName = extras.getString("restaurantName");
        restaurantTitle = (TextView) findViewById(R.id.restaurant_name);
        getSupportActionBar().setTitle(restaurantName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        createViews();
    }

    private void createViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MenuListAdapter(menus, MenuList.this);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
