package com.example.leonardo.testgoomer.activities;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.leonardo.testgoomer.R;
import com.example.leonardo.testgoomer.adapters.RestaurantListAdapter;
import com.example.leonardo.testgoomer.model.Restaurant;

import java.util.ArrayList;

public class RestaurantList extends BaseActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Bundle extras;
    private ArrayList<Restaurant> restaurants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);

        //Retrive the bundle information.
        extras = getIntent().getExtras();
        restaurants = (ArrayList<Restaurant>) extras
                .getSerializable(String.valueOf(R.string.saved_restaurants));

        createViews();
    }

    private void createViews() {

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager) mLayoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mAdapter = new RestaurantListAdapter(restaurants, RestaurantList.this);
        mRecyclerView.setAdapter(mAdapter);

    }
}
