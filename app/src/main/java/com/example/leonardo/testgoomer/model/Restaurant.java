package com.example.leonardo.testgoomer.model;

import java.io.Serializable;
import java.util.List;

public class Restaurant implements Serializable {
    private int id;
    private String name;
    private String address;
    private String image;
    private List<RestaurantMenu> restaurantMenu;

    public Restaurant(int id, String name, String address, String image) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<RestaurantMenu> getRestaurantMenu() {
        return restaurantMenu;
    }

    public void setRestaurantMenu(List<RestaurantMenu> restaurantMenu) {
        this.restaurantMenu = restaurantMenu;
    }
}
